#ifndef SENSOR_H
#define SENSOR_H

#include <Arduino.h>
#include <LittleFS.h>

#define CALLBACK_SENSOR_TYPE std::function<String()>

class Sensor
{
private:
    String id;
    String name;
    String type;
    String value;
    long timeout;
    int deadband;
    unsigned long lastSendTime;
    int sentqueuen = 0;
    int queuen = 0;
    CALLBACK_SENSOR_TYPE callback;

public:
    Sensor(String id, String name, String type, String value, long timeout, int deadband, CALLBACK_SENSOR_TYPE callback);
    ~Sensor();
    String json();
    String getId();
    String getType();
    String getValue();
    void setTimeout(long timeout);
    long getTimeout();
    void setDeadband(int deadband);
    int getDeadband();
    unsigned long getLastSendTime();
    void setLastSendTime(unsigned long lastSendTime);
    String callCallback();

    String readFile(String filename, time_t &ctime);
    void deleteFile(String filename);
    void createFile(String filename, String filevalue);
    void setQueuen(int queuen);
    int getQueuen();
    void setSentqueuen(int queuen);
    int getSentqueuen();
};

#endif