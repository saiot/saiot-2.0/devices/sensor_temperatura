#include "Sensor.h"

Sensor::Sensor(String id, String name, String type, String value, long timeout, int deadband, CALLBACK_SENSOR_TYPE callback)
{
    this->id = id;
    this->name = name;
    this->type = type;
    this->value = value;
    this->timeout = timeout;
    this->deadband = deadband;
    this->lastSendTime = 0;
    this->callback = callback;
}
Sensor::~Sensor() {}

String Sensor::json()
{
    String json = "";

    json += "\"";
    json += id;
    json += "\"";
    json += ":{";

    json += "\"name\":\"";
    json += name;

    json += "\",\"type\":\"";
    json += type;

    json += "\",\"value\":";
    json += value;

    json += ",\"timeout\":";
    json += timeout;

    json += ",\"deadband\":";
    json += deadband;

    json += "}";

    return json;
}

String Sensor::getId()
{
    return id;
}

String Sensor::getType()
{
    return type;
}

String Sensor::getValue()
{
    return value;
}

void Sensor::setTimeout(long timeout)
{
    this->timeout = timeout;
}

long Sensor::getTimeout()
{
    return timeout;
}

void Sensor::setDeadband(int deadband)
{
    this->deadband = deadband;
}

int Sensor::getDeadband()
{
    return deadband;
}

unsigned long Sensor::getLastSendTime()
{
    return lastSendTime;
}

void Sensor::setLastSendTime(unsigned long lastSendTime)
{
    this->lastSendTime = lastSendTime;
}

String Sensor::callCallback()
{
    value = callback();
    return value;
}

String Sensor::readFile(String filename, time_t &ctime)
{
  File any;
  any  = LittleFS.open(filename ,"r");

  if (!any) 
  {
    Serial.println("readfile failed" + filename);
    return "";
  }

  String content;
  while (any.available())
  {
    content += (char)any.read();
  }
  ctime = any.getCreationTime();
  
  any.close();
  return content;
}

void Sensor::deleteFile(String filename)
{
    if(!LittleFS.remove(filename))
    {
        Serial.println("deleteFile Failed");
    }
}

void Sensor::createFile(String filename, String filevalue)
{
    File any;
    any  = LittleFS.open(filename,"w");

    if (!any) 
    {
        Serial.println("createFile failed" + filename);
        return;
    }

    any.print(filevalue);

    any.close();
}

void Sensor::setQueuen(int queuen)
{
    this->queuen = queuen;
}

int Sensor::getQueuen()
{
    return queuen;
}

void Sensor::setSentqueuen(int sentqueuen)
{
    this->sentqueuen = sentqueuen;
}

int Sensor::getSentqueuen()
{
    return sentqueuen;
}