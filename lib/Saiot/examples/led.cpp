#include <Arduino.h>

#include "Saiot.h"

#define LED_PIN 2

String deviceName = "LED";
String deviceClass = "lampada";
String deviceDescription = "LED do ESP8266";

Saiot saiot(deviceName, deviceClass, deviceDescription);

// Callback do atuador
String ledSwitchCallback(String value[]);

void setup()
{
  Serial.begin(9600);

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  Actuator *led = new Actuator("LedSwitch", "LED", "switch", {"false"}, ledSwitchCallback);
  saiot.addActuator(*led);
  saiot.start();
}

void loop()
{
  saiot.loop();
}

String ledSwitchCallback(String value[])
{
  String ledState = value[0];

  Serial.println("Recebido valor no atuador ledSwitch: " + ledState);

  if (ledState.compareTo("true") == 0)
  {
    digitalWrite(LED_PIN, LOW);
    return "true";
  }
  else
  {
    digitalWrite(LED_PIN, HIGH);
    return "false";
  }
}
